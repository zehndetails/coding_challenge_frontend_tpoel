import styled from 'styled-components';

const Headline = styled.h2`
  font-size: 24px;
  line-height: 32px;
`;

export default Headline;
