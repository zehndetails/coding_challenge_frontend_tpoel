import * as React from 'react';
import styled from 'styled-components';

import Headline from './Headline';

const Wrapper = styled.div`
  text-align: center;
  padding-bottom: 64px;
  margin-bottom: 64px;
  border-bottom: 1px solid #efefef;
  width: 100%;
`;

const Form = styled.div`
  display: inline-block;
  width: 250px;
`;

const Input = styled.input`
  display: block;
  width: 100%;
  padding: 10px;
  border-radius: 4px;
  font-size: 16px;
  margin-top: 20px;
`;

const Button = styled.button`
  padding: 10px;
  background-color: #ffffff;
  margin-top: 20px;
  display: block;
  width: 100%;
  border-radius: 4px;
  vertical-align: center;
  font-size: 16px;
  cursor: pointer;
  border-style: solid;
  border-width: 1px;
  border-color: #666666;
  color: #000000;
  &:hover {
    border-color: #27b379;
    color: #27b379;
  }
`;

const ErrorMessage = styled.p`
  background-color: #ffeeee;
  color: #000000;
  border-radius: 4px;
  margin: 16px 0;
  padding: 8px;
  border: 1px solid #aa0000;
`;

declare type Props = {
  onSubmit: (fields: {}) => void,
  fields: {},
  headline: string,
  buttonCopy: string,
  errorMessage: string
};
declare type State = {};

export default class FormView extends React.Component<Props, State> {
  state = {};

  onChange = fieldName => (event: SyntheticInputEvent<HTMLInputElement>) =>
    this.setState({ [fieldName]: event.target.value });

  onClick = (event: SyntheticInputEvent<HTMLDivElement>) => {
    this.props.onSubmit(this.state);
    return this.props.fields.forEach(field =>
      this.setState({ [field.name]: '' })
    );
  };

  render() {
    return (
      <Wrapper>
        <Form>
          <Headline>{this.props.headline}</Headline>
          {this.props.fields.map(field => (
            <Input
              key={field.name}
              type={field.type}
              value={this.state[field.name] || ''}
              onChange={this.onChange(field.name)}
              placeholder={field.placeholder}
            />
          ))}
          {this.props.errorMessage && (
            <ErrorMessage>{this.props.errorMessage}</ErrorMessage>
          )}
          <Button onClick={this.onClick}>{this.props.buttonCopy}</Button>
        </Form>
      </Wrapper>
    );
  }
}
