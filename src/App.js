/* @flow */
import * as React from 'react';
import { injectGlobal } from 'styled-components';
import styledNormalize from 'styled-normalize';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Nav from './containers/Nav/';
import NotFound from './containers/NotFound/';
import Signup from './containers/Signup/';
import PosterManager from './containers/PosterManager/';

import PageWrapper from './components/PageWrapper';

injectGlobal`
  ${styledNormalize}

  html {
    box-sizing: border-box;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }

  body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  }
`;

declare type State = {
  loginUser: ?MorressierUser
};

export default class App extends React.Component<{}, State> {
  state = {
    loginUser: null
  };

  login = (loginUser: MorressierUser) => {
    this.setState({
      loginUser
    });
  };

  logout = () => {
    this.setState({ loginUser: null });
    window.location = '/';
  };

  render() {
    return (
      <Router>
        <PageWrapper>
          <Nav loginUser={this.state.loginUser} logout={this.logout} />
          <Switch>
            <Route
              exact
              path="/"
              render={routeProps => (
                <Signup history={routeProps.history} login={this.login} />
              )}
            />
            <Route path="/posters" component={PosterManager} />
            <Route component={NotFound} />
          </Switch>
        </PageWrapper>
      </Router>
    );
  }
}
