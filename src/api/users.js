import { BASE_URL } from './config';
const USERS_URL = `${BASE_URL}/users`;

export const getViaEmail = (email: string): Promise<MorressierUser[]> =>
  fetch(`${USERS_URL}?email=${email}`).then(response => response.json());

export const post = (requestParams: {
  email: string,
  name: string
}): Promise<MorressierPoster> =>
  fetch(USERS_URL, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(requestParams)
  }).then(response => response.json());

export const get = (id: string): Promise<MorressierPoster> =>
  fetch(`${USERS_URL}/${id}`).then(response => response.json());

export const put = (
  id: string,
  requestParams: {
    title?: string,
    name?: string,
    email?: string
  }
): Promise<MorressierPoster> =>
  fetch(`${USERS_URL}/${id}`, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'PUT',
    body: JSON.stringify(requestParams)
  }).then(response => response.json());

export const del = (id: string): Promise<void> =>
  fetch(`${USERS_URL}/${id}`, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'DELETE'
  }).then(response => response.json());
