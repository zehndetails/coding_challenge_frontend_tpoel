import { BASE_URL } from './config';
const POSTERS_URL = `${BASE_URL}/posters`;

export const getAll = (): Promise<MorressierPoster[]> =>
  fetch(POSTERS_URL).then(response => response.json());

export const post = (requestParams: {
  title?: string,
  author?: string
}): Promise<MorressierPoster> =>
  fetch(`${POSTERS_URL}`, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(requestParams)
  }).then(response => response.json());

export const get = (id: string): Promise<MorressierPoster> =>
  fetch(`${POSTERS_URL}/${id}`).then(response => response.json());

export const put = (
  id: string,
  requestParams: {
    title?: string,
    author?: string
  }
): Promise<MorressierPoster> =>
  fetch(`${POSTERS_URL}/${id}`, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'PUT',
    body: JSON.stringify(requestParams)
  }).then(response => response.json());

export const del = (
  id: string,
  requestParams: {
    user_id: string
  }
): Promise<void> =>
  fetch(`${POSTERS_URL}/${id}`, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'DELETE',
    body: JSON.stringify(requestParams)
  }).then(response => response.json());
