import React from 'react';
import ReactDOM from 'react-dom';
import Nav from './index';
import Logo from './Logo';

import { expect } from 'chai';
import { shallow } from 'enzyme';

describe('The Navbar container', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Nav />, div);
  });

  it('renders the Logo component', () => {
    const wrapper = shallow(<Nav />);
    expect(wrapper.containsAllMatchingElements([<Logo />])).to.equal(true);
  });
});
