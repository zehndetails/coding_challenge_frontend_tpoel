/* @flow */
import * as React from 'react';
import styled from 'styled-components';

import Logo from './Logo';
import logoSrc from './morressier_logo.svg';

const UserBlock = styled.div`
  display: inline-block;
  float: right;
  text-align: right;
`;

const Nav = (props: { loginUser: ?MorressierUser, logout: () => void }) => (
  <div>
    <Logo src={logoSrc} alt="Morressier" />
    {props.loginUser && (
      <UserBlock>
        {props.loginUser.name} | <button onClick={props.logout}>log out</button>
      </UserBlock>
    )}
  </div>
);

export default Nav;
