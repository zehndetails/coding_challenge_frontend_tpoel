import styled from 'styled-components';

const Logo = styled.img`
  margin-bottom: 20px;
  height: 40px;
`;

export default Logo;
