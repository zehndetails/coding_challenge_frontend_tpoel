import React from 'react';
import PosterManager from './index';
import CreatePoster from './CreatePoster';
import PosterList from './PosterList';
import { BASE_URL } from '../../api/config';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import fetchMock from 'fetch-mock';

fetchMock.get(`${BASE_URL}/posters`, [
  {
    id: '5a527401eaf79100092bc455',
    title: 'How I Learned to Stop Worrying and Love the Bomb',
    author: '5a527400eaf7910008557ad4'
  },
  {
    id: '5a5274b8eaf7910008557ad6',
    title: 'Where the Wild Roses Grow',
    author: '5a5274b7eaf7910008557ad5'
  }
]);

fetchMock.get(`${BASE_URL}/users/5a5274b7eaf7910008557ad5`, {
  id: '5a5274b7eaf7910008557ad5',
  email: 'nick@cave.com',
  title: null,
  name: 'Nick Cave',
  roles: ['author'],
  posters: ['5a5274b8eaf7910008557ad6']
});

fetchMock.get(`${BASE_URL}/users/5a527400eaf7910008557ad4`, {
  id: '5a527400eaf7910008557ad4',
  email: 'strange@love.com',
  title: null,
  name: 'Dr. Strangelove',
  roles: ['author'],
  posters: ['5a527401eaf79100092bc455']
});

describe('The PosterManager', () => {
  it('renders the Create Poster component and the Poster List', async () => {
    const wrapper = shallow(<PosterManager />);
    await wrapper.instance().componentDidMount();
    expect(
      wrapper.containsAllMatchingElements([<CreatePoster />, <PosterList />])
    ).to.equal(true);
  });

  it('should be empty at first', () => {
    const wrapper = shallow(<PosterManager />);
    expect(wrapper.state('posters')).to.eql([]);
  });

  it('passes updatePoster and deletePoster to PosterList', () => {
    const wrapper = shallow(<PosterManager />);
    const posterList = wrapper.find(PosterList);
    const updatePoster = wrapper.instance().updatePoster;
    const deletePoster = wrapper.instance().deletePoster;
    expect(posterList.prop('updatePoster')).to.eql(updatePoster);
    expect(posterList.prop('deletePoster')).to.eql(deletePoster);
  });

  it('passes createPoster to CreatePoster', () => {
    const wrapper = shallow(<PosterManager />);
    const createPosterComp = wrapper.find(CreatePoster);
    const createPosterFunc = wrapper.instance().createPoster;
    expect(createPosterComp.prop('createPoster')).to.eql(createPosterFunc);
  });
});
