import * as React from 'react';
import FormView from '../../components/FormView';

declare type Props = {
  createPoster: (email: string) => void
};

export default class CreatePoster extends React.Component<Props> {
  onSubmit = (fields: { email: string, title: string }) =>
    this.props.createPoster(fields);

  render() {
    return (
      <FormView
        headline="Create Poster & Author"
        buttonCopy="+ Add"
        fields={[
          { type: 'text', name: 'title', placeholder: 'Poster Title' },
          { type: 'email', name: 'email', placeholder: 'Email address' }
        ]}
        onSubmit={this.onSubmit}
      />
    );
  }
}
