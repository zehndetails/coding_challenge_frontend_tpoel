import styled from 'styled-components';

const DeleteButton = styled.button.attrs({ children: 'Delete' })`
  display: inline-block;
  width: 100px;
  cursor: pointer;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  border: 1px solid #333333;
  background-color: #ffffff;
  border-radius: 4px;
  &:hover {
    color: #aa0000;
    border-color: #aa0000;
  }
`;

export default DeleteButton;
