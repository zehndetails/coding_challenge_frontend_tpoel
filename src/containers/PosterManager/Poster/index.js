/* @flow */
import * as React from 'react';
import styled from 'styled-components';

import { users } from '../../../api';

import TableInput from './TableInput';
import DeleteButton from './DeleteButton';

const Wrapper = styled.div`
  font-size: 16px;
  display: block;
  width: 100%;
  padding: 10px;
  white-space: nowrap;
  background-color: transparent;
  border-radius: 4px;
  button {
    display: none;
  }
  &:hover {
    button {
      display: inline-block;
    }
    background-color: #fafafa;
  }
`;

const Column = styled.div`
  display: inline-block;
  width: ${props => props.width};
`;

declare type State = {
  user: ?MorressierUser,
  name: ?string,
  email: ?string,
  title: ?string
};

declare type Props = {
  poster: MorressierPoster,
  deletePoster: (poster: MorressierPoster) => Promise<void>,
  updatePoster: (
    poster: MorressierPoster,
    fields: { title: string }
  ) => Promise<void>
};

export default class Poster extends React.Component<Props, State> {
  state = {
    user: null,
    name: null,
    email: null,
    title: null
  };

  async componentDidMount() {
    this.setState({
      user: await users.get(this.props.poster.author)
    });
  }

  onChange = (fieldName: 'name' | 'email' | 'title') => (
    event: SyntheticInputEvent<HTMLInputElement>
  ) =>
    this.setState({
      [fieldName]: event.target.value
    });

  onBlur = (fieldName: 'name' | 'email' | 'title') => (
    event: SyntheticInputEvent<HTMLInputElement>
  ) => {
    if (fieldName === 'title' && this.state[fieldName]) {
      return this.props.updatePoster(this.props.poster, {
        [fieldName]: this.state[fieldName]
      });
    }
    if (this.state.user && this.state[fieldName]) {
      return this.updateUser(this.state.user, {
        [fieldName]: this.state[fieldName]
      });
    }
  };

  updateUser = async (
    user: MorressierUser,
    fields: { name?: string, email?: string }
  ) => {
    await users.put(user.id, fields);
    return this.setState({
      user: {
        ...this.state.user,
        ...fields
      }
    });
  };

  deletePoster = (event: SyntheticInputEvent<HTMLButtonElement>) =>
    this.props.deletePoster(this.props.poster);

  render() {
    return (
      <Wrapper>
        <Column width="15%">
          <TableInput
            onChange={this.onChange('name')}
            onBlur={this.onBlur('name')}
            value={
              this.state.name || (this.state.user && this.state.user.name) || ''
            }
          />
        </Column>
        <Column width="25%">
          <TableInput
            onChange={this.onChange('email')}
            onBlur={this.onBlur('email')}
            value={
              this.state.email ||
              (this.state.user && this.state.user.email) ||
              ''
            }
          />
        </Column>
        <Column width="calc(60% - 100px)">
          <TableInput
            weight="bold"
            onChange={this.onChange('title')}
            onBlur={this.onBlur('title')}
            value={this.state.title || this.props.poster.title}
          />
        </Column>
        <Column width="100%">
          <DeleteButton onClick={this.deletePoster} />
        </Column>
      </Wrapper>
    );
  }
}
