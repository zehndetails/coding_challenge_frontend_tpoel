import styled from 'styled-components';

const TableInput = styled.input`
  border: 1px solid transparent;
  width: 90%;
  background-color: transparent;
  font-weight: ${props => props.weight || 'normal'};
`;

export default TableInput;
