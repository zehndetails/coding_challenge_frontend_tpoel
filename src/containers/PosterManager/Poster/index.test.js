import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import fetchMock from 'fetch-mock';

import Poster from './index';
import TableInput from './TableInput';
import DeleteButton from './DeleteButton';
import { BASE_URL } from '../../../api/config';

fetchMock.get(`${BASE_URL}/users/5a5274b7eaf7910008557ad5`, {
  id: '5a5274b7eaf7910008557ad5',
  email: 'nick@cave.com',
  title: null,
  name: 'Nick Cave',
  roles: ['author'],
  posters: ['5a5274b8eaf7910008557ad6']
});

describe('The Poster', () => {
  it('renders TableInputs and a DeleteButton', async () => {
    const wrapper = shallow(
      <Poster
        poster={{
          id: '5a5274b8eaf7910008557ad6',
          title: 'Where the Wild Roses Grow',
          author: '5a5274b7eaf7910008557ad5'
        }}
      />
    );
    await wrapper.instance().componentDidMount();
    expect(
      wrapper.containsAllMatchingElements([<TableInput />, <DeleteButton />])
    ).to.equal(true);
  });
});
