/* @flow */
import * as React from 'react';

import PosterList from './PosterList';
import CreatePoster from './CreatePoster';

import { posters, users } from '../../api';

declare type State = {
  posters: MorressierPoster[]
};

export default class Posters extends React.Component<{}, State> {
  state = {
    posters: []
  };

  async componentDidMount() {
    return this.setState({
      posters: await posters.getAll()
    });
  }

  createPoster = async (params: { email: string, title: string }) => {
    const userResponse = await users.getViaEmail(params.email);
    let user = userResponse[0];
    if (!user || !user.id) {
      user = await users.post({
        email: params.email,
        name: '(add name)'
      });
    }

    this.setState({
      posters: [
        ...this.state.posters,
        await posters.post({
          author: user.id,
          title: params.title
        })
      ]
    });
  };

  deletePoster = async (poster: MorressierPoster) => {
    const response = await posters.del(poster.id, { user_id: poster.author });
    if (!response) {
      const posterIndex = this.state.posters.map(x => x.id).indexOf(poster.id);
      if (posterIndex >= 0) {
        this.setState({
          posters: this.state.posters
            .slice(0, posterIndex)
            .concat(this.state.posters.slice(posterIndex + 1))
        });
      }
    }
  };

  updatePoster = async (
    poster: MorressierPoster,
    fields: { title: string }
  ) => {
    await posters.put(poster.id, fields);
    const posterIndex = this.state.posters.map(x => x.id).indexOf(poster.id);
    if (posterIndex >= 0) {
      return this.setState({
        posters: this.state.posters
          .slice(0, posterIndex)
          .concat({
            ...this.state.posters[posterIndex],
            ...fields
          })
          .concat(this.state.posters.slice(posterIndex + 1))
      });
    }
  };

  render() {
    return (
      <div>
        <CreatePoster createPoster={this.createPoster} />
        <PosterList
          posters={this.state.posters}
          updatePoster={this.updatePoster}
          deletePoster={this.deletePoster}
        />
      </div>
    );
  }
}
