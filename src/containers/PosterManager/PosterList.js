/* @flow */
import * as React from 'react';

import Headline from '../../components/Headline';

import Poster from './Poster/';

declare type Props = {
  posters: MorressierPoster[],
  updatePoster: (
    poster: MorressierPoster,
    fields: { title: string }
  ) => Promise<void>,
  deletePoster: (poster: MorressierPoster) => Promise<void>
};

export default class PosterList extends React.Component<Props> {
  render() {
    return (
      <div>
        <Headline>Existing Posters</Headline>
        {this.props.posters
          .slice()
          .reverse()
          .map(poster => (
            <Poster
              key={poster.id}
              poster={poster}
              deletePoster={this.props.deletePoster}
              updatePoster={this.props.updatePoster}
            />
          ))}
      </div>
    );
  }
}
