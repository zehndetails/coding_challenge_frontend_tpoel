/* @flow */
import * as React from 'react';

import { users } from '../../api/';

import FormView from '../../components/FormView';
import type { RouterHistory } from 'react-router-dom';

declare type Props = {
  login: (user: MorressierUser) => void,
  history: RouterHistory
};

declare type State = {
  error: ?string
};

export default class Signup extends React.Component<Props, State> {
  state = {
    error: null
  };

  onSubmit = async (fields: { email: string, title: string, name: string }) => {
    this.setState({
      error: null
    });
    const userResponse = await users.getViaEmail(fields.email);
    let user = userResponse[0];
    if (!user || !user.id) {
      user = await users.post({
        email: fields.email,
        name: fields.name,
        title: fields.title
      });
    }

    if (user.id) {
      this.props.login(user);
      return this.props.history.push('/posters');
    }
    return this.setState({
      error:
        (user.message && user.message.title) ||
        user.message ||
        'Error, please try again!'
    });
  };

  render() {
    return (
      <FormView
        headline="Create Organiser"
        buttonCopy="Create"
        errorMessage={this.state.error}
        fields={[
          { type: 'text', name: 'title', placeholder: 'Title' },
          { type: 'text', name: 'name', placeholder: 'Name' },
          { type: 'email', name: 'email', placeholder: 'Email address' }
        ]}
        onSubmit={this.onSubmit}
      />
    );
  }
}
