/* @flow */
import * as React from 'react';

const NotFound = (props: {
  location: {
    pathname: string
  }
}) => (
  <h3>
    The URL <code>{props.location.pathname}</code> doesn't exist.
  </h3>
);

export default NotFound;
