import React from 'react';
import ReactDOM from 'react-dom';
import NotFound from './index';

describe('The NotFound container', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<NotFound location={{ pathname: '/404' }} />, div);
  });
});
