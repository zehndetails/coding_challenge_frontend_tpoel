declare type MorressierUserRole = 'organiser' | 'author';

declare type MorressierUser = {
  id: string,
  email: string,
  title: string,
  name: string,
  roles: MorressierUserRole[]
};
